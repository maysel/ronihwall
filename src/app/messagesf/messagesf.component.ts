import { MessagesService } from './../messages/messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'messagesf',
  templateUrl: './messagesf.component.html',
  styleUrls: ['./messagesf.component.css']
})
export class MessagesfComponent implements OnInit {

  messages;
  response;
  constructor(private service:MessagesService) { }

  ngOnInit() {
    this.service.getMessagesFire().subscribe(response=>{
      console.log(response);
      this.messages = response;
    })
  }

}
