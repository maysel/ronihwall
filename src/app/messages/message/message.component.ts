import { MessagesService } from './../messages.service';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'; // כדי לשמור את האיידיי

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message; // תכונה בשם מסאג
  constructor(private route:ActivatedRoute, private service:MessagesService) { }

  ngOnInit() {
    // כך נקרא את האיידיי מהיואראל
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      // לקרוא לנתונים מהשרת
      this.service.getMessage(id).subscribe(response=>{
        this.message = response.json(); // הגייסון הופכת את הסטרינג לגייסון
        console.log(this.message);
      })
    });
  }

}
