import { MessagesService } from './messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  //messages = ['Message1','Message2','Message3','Message4'];
  messages;
  messagesKeys = [];

  constructor(private service:MessagesService) {
    service.getMessages().subscribe(response=>{ // הגט מסג מחזיר אובזרבל ולו צריך להגיד סאבסקרייב כלומר להירשם אליו
      //console.log(response.json()));
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages); // output of all keys
    });
  }

  optimisticAdd(message){
    // did this line just to make sure everything works, we checked by F12, the console, --->> console.log("addMessage worked " + message)
    var newKey = this.messagesKeys[this.messagesKeys.length-1] + 1;
    var newMessageObject = {}; // because its array of objects
    newMessageObject['body'] = message;
    this.messages[newKey] = newMessageObject;
    this.messagesKeys = Object.keys(this.messages); // we want to refresh again all messageskeys, and refresh the *ngfor to show all updated keys
  }

  pessimisticAdd(){
    console.log("blabla");
    // we copy from the constructor, added this.
    this.service.getMessages().subscribe(response=>{ // הגט מסג מחזיר אובזרבל ולו צריך להגיד סאבסקרייב כלומר להירשם אליו
      //console.log(response.json()));
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages); // output of all keys

     });
     }

  deleteMessage(key){
    console.log(key);
    //optimistic delete
    let index = this.messagesKeys.indexOf(key);
    this.messagesKeys.splice(index,1); // 1 num of elements to remove
    //delete from server
    this.service.deleteMessage(key).subscribe();
    response=>console.log(response);

  }

  ngOnInit() {
  }

  }
