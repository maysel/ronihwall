import { MessagesService } from './../messages.service';
import { Component, OnInit , Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'messagesForm',
  templateUrl: './messages-form.component.html',
  styleUrls: ['./messages-form.component.css']
})
export class MessagesFormComponent implements OnInit {

  //we build here the comunication between msg form to his father -> messages
  @Output() addMessage:EventEmitter<any> = new EventEmitter<any>();
  @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>();

  service:MessagesService;
  msgform = new FormGroup({ // אנחנו יוצרים מבנה נתונים שהוא הביטוי בקוד של האיגטיאמאל.ברגע שנתחיל למלא נתונים בטופס אז הנתונים האלו יישמרו בקוד כך
    message:new FormControl(), // נוכל גם להכניס לסוגרים האלו חוקי ואלידציה
    user:new FormControl(), // מכאן קישרנו בקובץ האיגטיאמל
  });


  sendData(){ // לוקח את הערכים שרשמנו בטופס ושומר בקונסול
    //this is how son and father comunicate, just: ONLY SON AND FATHER 
    this.addMessage.emit(this.msgform.value.message); 

    console.log(this.msgform.value);
    this.service.postMessage(this.msgform.value).subscribe(
      response =>{
        console.log(response.json())
        this.addMessagePs.emit(); // here theres no input beacuse this is Pesimist - just timeing the event. WE ARE IN THE RESPONE WHICH MEANS THE DATA IN INSIDE
      }
    )
  };

  constructor(service:MessagesService) { 
    this.service = service;
  }

  ngOnInit() {
  }

}
