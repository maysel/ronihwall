import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {HttpParams} from  '@angular/common/http';
import {AngularFireDatabase} from 'angularFire2/database';

import 'rxjs/Rx';

@Injectable()
export class MessagesService {
  http:Http;
  getMessages(){
    //return ['Message1','Message2','Message3'];
    //get messages from SLIM rest API, dont say DB!!
    // add the JWT
    let token = localStorage.getItem('token');
    // add the TOKEN to the header of the request
    let options = {
      headers: new Headers({
        'Authorization':'Bearer '+token
      })
    }
    return this.http.get( environment.url +'messages',options);
  }


// this funcation contact to the db of fire base and take all the msg that are there
  getMessagesFire(){ // list can take all object that in some NG.. valueChanges return OBZERBALE!!
    return this.db.list('/messages').valueChanges();
  }
  
  
  getMessage(id){
    return this.http.get(environment.url + 'messages/'+id);

  }


  postMessage(data){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded' // הגדרנו דרך מחלקה של אנגולר שמיועדת לתהייחסות של ההאדר, אחרת הנתונים לא יעבור לסלימ
      })
    }
    let params = new HttpParams().append('message',data.message); // פארמס הוא מבנה נתונים שמחזיק קיי ו ואליו, הקיי זה מסג ו ואליו זה דטה.מסג
    return this.http.post(environment.url + 'messages',
  params.toString(), options); // הוספנו אופטינס זה אומר שהקידוד שזה יהיה זה מה שהוספנו שם, אחרת זה לא יגיע לסלים בצורה הנכונה

}


deleteMessage(key){
  return this.http.delete(environment.url + 'messages/'+key)
}


login(credentials){ 
  let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded' // הגדרנו דרך מחלקה של אנגולר שמיועדת לתהייחסות של ההאדר, אחרת הנתונים לא יעבור לסלימ
      })
    } // מתכוננים למשלוח לשרת, שולחים כאן 2 פרמטרים שיגעו לפונקציה דרך הקרידנצלז
    let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    return this.http.post(environment.url + 'auth',params.toString(),options).map(response=> { // חייבים להעביר לשרת בצורה של סטרינג ולא אובייקט
      let token = response.json().token;
      if (token) localStorage.setItem('token', token);// we take the UN & PSW, we send to the routh that call auth and from what came back we took the token and ask if arrivad well, if so we save it in the localstorgh
      console.log(token);    
    }); 
   
}

  constructor(http:Http,private db:AngularFireDatabase) {
    this.http = http;
   }

}
