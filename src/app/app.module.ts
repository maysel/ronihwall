import { environment } from './../environments/environment';
import {RouterModule} from '@angular/router';
import { UsersService } from './users/users.service';
import {HttpModule} from '@angular/http';
import { MessagesService } from './messages/messages.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { UsersComponent } from './users/users.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';

import {FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { LoginComponent } from './login/login.component';
import {AngularFireModule} from 'angularFire2';
import {AngularFireDatabaseModule} from 'angularFire2/database';
import { MessagesfComponent } from './messagesf/messagesf.component';


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent,
    MessagesFormComponent,
    NavigationComponent,
    NotFoundComponent,
    MessageComponent,
    LoginComponent,
    MessagesfComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase), // האתחול מול פיירבייס
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      // here we'll define all moudules 
      //define route by object and 2 elements
      {path:'',component:MessagesComponent},
      {path:'users',component:UsersComponent},
      {path:'message/:id',component:MessageComponent},
      {path:'login',component:LoginComponent},
      {path:'messagesf',component:MessagesfComponent},
      {path:'**',component:NotFoundComponent}
    ])

  ],
  providers: [
    MessagesService,
    UsersService
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
