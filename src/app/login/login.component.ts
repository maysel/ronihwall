import { Router } from '@angular/router';
import { MessagesService } from './../messages/messages.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalid = false;
  loginform = new FormGroup({
    user:new FormControl(), // נוכל גם להכניס לסוגרים האלו חוקי ואלידציה
    password:new FormControl(),
  });

  login (){
    this.service.login(this.loginform.value).subscribe(response=>{
      this.router.navigate(['/']);
    },error => {
      this.invalid = true;
    })
  }

  logout(){
    localStorage.removeItem('token');
    this.invalid = false;
  }
  constructor(private service:MessagesService,private router:Router) { }

  ngOnInit() {
  }

}
